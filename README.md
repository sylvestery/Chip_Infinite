# Chip _Infinite_

Simple Chip 8 Emulator made in java.
**WARNING** - Emulator providied as is.

## Compatibility

* Video Output Functions
* No Sound Emulation 

## Known Bugs

### Emulation

* CPU Cycle Emulation is too fast.

* Screen Draw Errors
  * Tetris and others crash when sprites go off screen.
  * Sprites in Space Invader and Possibly Others Wrap unintentionally around screen.

### UI

* Crash if incorrect rom file is selected by user.
* Absolute path is the only way to read rom from terminal.

## Future Goals 

* Reach 100% Compatibility with publicly  available Chip 8 ROMs.
* Fix Screen Draw Issues.
* Add support for SuperChip 8 and its games.
* Implement Proper Menu System.

## References

[How to Write and Emulator](http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/)

[Introduction to CHIP-8](http://www.emulator101.com/introduction-to-chip-8.html)