import com.Addiction00.ChipInfinite.Core.Hardware.Memory;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class MemoryTest {
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(Memory.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }
    private Memory memory = new Memory();
    @org.junit.Test
    public void readByte(int i) {
        System.out.println(memory.read(i));
    }

    @org.junit.Test
    public void writeByte(int location) {
        memory.write(3, location);
        readByte(location);
    }

    public void runBoth(){
        writeByte(2);
        readByte(2);
    }

}
