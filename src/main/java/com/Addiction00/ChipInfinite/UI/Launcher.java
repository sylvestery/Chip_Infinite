package com.Addiction00.ChipInfinite.UI;

import com.Addiction00.ChipInfinite.Core.Emulator.Emulator;
import com.Addiction00.ChipInfinite.Core.Emulator.EmulatorRunnable;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class Launcher extends Application {
    private FileChooser fileChooser = new FileChooser();
    private static String filePath = "dont";
    public static void main(String[] args) {
        if(args.length != 0 ) {
            filePath = args[0];
        }
        launch();
    }

    private void makeFileDialog() {
        fileChooser.setTitle("Pick CHIP 8 Rom");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Infinite");


        makeFileDialog();
        Group root = new Group();
        Canvas canvas = new Canvas(640, 320);
        root.getChildren().addAll(canvas);
        Scene scene = new Scene(root);
        Emulator emulator = new Emulator(scene, canvas);
        primaryStage.setScene(scene);
        File rom;
        if(!filePath.isEmpty() && !new File(filePath).exists()){
            rom = fileChooser.showOpenDialog(primaryStage);
        } else {
            rom = new File(filePath);
        }

        emulator.loadRom(rom);
        emulator.start();
        EmulatorRunnable runnable = new EmulatorRunnable(scene, canvas, emulator);
        Thread t = new Thread(runnable);
        t.start();
        primaryStage.show();
    }


}
