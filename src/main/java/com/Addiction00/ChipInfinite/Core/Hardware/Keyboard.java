package com.Addiction00.ChipInfinite.Core.Hardware;

import javafx.scene.Scene;

public class Keyboard {
    private byte key;


    public Keyboard() {
        key = -1;
    }

    byte getKey() {
        return key;
    }

    public void setKey(byte key) {
        this.key = key;
    }

    public String[] debugKeys(Scene scene, String[] args) {
        scene.setOnKeyPressed(event ->
        {
            switch (event.getCode()) {
                case PERIOD:
                    if (args[0].equals("Wrap")) {
                        args[0] = "Unwrap";
                    } else {
                        args[0] = "Wrap";
                    }
                    break;
            }
        });
        return args;

    }

    public void setKeyWithPress(Scene scene) {
        scene.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case DIGIT1:
                    key = 0x0;
                    break;
                case DIGIT2:
                    key = 0x1;
                    break;
                case DIGIT3:
                    key = 0x2;
                    break;
                case DIGIT4:
                    key = 0xC;
                    break;
                case Q:
                    key = 0x4;
                    break;
                case W:
                    key = 0x5;
                    break;
                case E:
                    key = 0x6;
                    break;
                case R:
                    key = 0xD;
                    break;
                case A:
                    key = 0x7;
                    break;
                case S:
                    key = 0x8;
                    break;
                case D:
                    key = 0x9;
                    break;
                case F:
                    key = 0xE;
                    break;
                case Z:
                    key = 0xA;
                    break;
                case X:
                    key = 0x0;
                    break;
                case C:
                    key = 0xB;
                    break;
                case V:
                    key = 0xF;
                    break;
            }
        });


    }

    public void setKeyRelease(Scene scene) {
        scene.setOnKeyReleased(event -> key = -1);
    }
}
