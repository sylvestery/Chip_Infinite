package com.Addiction00.ChipInfinite.Core.Hardware;

import java.util.Random;

public class CPU {
    public static final int PROGRAM_COUNTER_START = 0x200;
    private short opCode;
    private short[] VERegisters = new short[16];
    private short I;
    private short pc;
    private short[] stack = new short[16];
    private short sp;
    private long cpuTime;
    private boolean drawFlag;
    private short delayTimer;
    private short soundTimer;
    private Memory memory;
    private Screen screen;
    private Keyboard keyboard;
    private boolean verticalWrap;
    private int cycleCount = 0;

    public CPU(Memory memory, Screen screen, Keyboard keyboard) {
        this.memory = memory;
        this.screen = screen;
        this.keyboard = keyboard;
        this.drawFlag = false;
        this.cpuTime = System.currentTimeMillis();
        this.verticalWrap = true;
        reboot();
    }

    public void setVerticalWrap(boolean verticalWrap) {
        this.verticalWrap = verticalWrap;
    }

    public long getCpuTime() {
        return cpuTime;
    }

    public void setCpuTime(long cpuTime) {
        this.cpuTime = cpuTime;
    }

    public void decrementTimers() {
        if (cycleCount == 60) {

            if (delayTimer > 0) {
                delayTimer--;
            }

            if (soundTimer > 0) {
                System.out.println("Sound Played.");
                soundTimer--;
            }
            cycleCount = 0;
        }
    }

    public void emulateCycle() {
        fetchNextOPcode();
        runNextInstruction();
        decrementTimers();
        cycleCount++;

    }

    public void setFontSpace(short[] fontSpace) {

        for (int i = 0; i < 0x50; i++) { //Sets 0 - 80
            memory.write(fontSpace[i], i);
        }
    }


    public boolean isDrawFlag() {
        return drawFlag;
    }

    public void setDrawFlag(boolean drawFlag) {
        this.drawFlag = drawFlag;
    }

    public void reboot() {
        pc = PROGRAM_COUNTER_START;
        opCode = 0;
        I = 0;
        sp = 0;

    }


    private void fetchNextOPcode() {
        opCode = (short) (memory.read(pc) << 8 | memory.read(pc + 1));
    }

    public int getVx() {
        return (this.opCode & 0x0F00) >> 8;
    }

    public int getVy() {
        return (this.opCode & 0x00F0) >> 4;
    }


    private void runNextInstruction() {
        System.out.println(Integer.toHexString(opCode));
        final int F = 0xF;
        short nnn = (short) (opCode & 0x0FFF);
        short kk = (short) (opCode & 0x00FF);
        int x = getVx();
        int y = getVy();
        switch (opCode & 0xF000) {
            case 0x0000:
                switch (opCode & 0x000F) {
                    case 0x0000: //0x00E0 Clears Screen
                        screen.clear();
                        drawFlag = true;
                        pc += 2;
                        break;
                    case 0x000E: //Returns from last subroutine.
                        sp--;
                        pc = stack[sp];
                        pc += 2;
                        break;
                    default:
                        System.out.printf("Unknown 0x0000 OPCODE: 0x%s", opCode);
                        break;
                }
                break;

            case 0x1000: // 1NNN Goes to instruction at NNN
                pc = nnn;
                break;
            case 0x2000: // 2NNN Next subroutine at NNN.
                stack[sp] = pc;
                sp++;
                pc = nnn;
                break;
            case 0x3000:// 3xkk Skips next instruction If Vx does equal kk.
                if (VERegisters[x] == kk) {
                    pc += 4;
                } else {
                    pc += 2;
                }
                break;
            case 0x4000: // 4xkk Skips next instruction If Vx does not equal kk.
                if (VERegisters[x] != kk) {
                    pc += 4;
                } else {
                    pc += 2;
                }
                break;
            case 0x5000: // Skips next instruction if Vx equals Vy
                if (VERegisters[x] == VERegisters[y]) {
                    pc += 4;
                } else {
                    pc += 2;
                }
                break;
            case 0x6000: //6Xkk set Vx to kk
                VERegisters[x] = kk;
                pc += 2;
                break;
            case 0x7000: //7xkk add kk to Vx
                VERegisters[x] += kk;
                pc += 2;
                break;
            case 0x8000: //
                switch (opCode & 0x000F) {
                    case 0x0000: // 8xy0 Sets Vx equal to Vy
                        VERegisters[x] = VERegisters[y];
                        pc += 2;
                        break;
                    case 0x0001: // 8xy1 Logical OR x and y
                        VERegisters[x] |= VERegisters[y];
                        pc += 2;
                        break;
                    case 0x0002: // 8XY2 Logical AND Vx and Vy
                        VERegisters[x] &= VERegisters[y];
                        pc += 2;
                        break;
                    case 0x0003: // 8xy3 Logical Xor Vx and Vy
                        VERegisters[x] ^= VERegisters[y];
                        pc += 2;
                        break;
                    case 0x0004: // 8XY4 Add Vx, Vy
                        if (VERegisters[y] > (0xFF - VERegisters[x])) {
                            VERegisters[0xF] = 1; //0xF is 15
                        } else {
                            VERegisters[0xF] = 0;
                        }
                        VERegisters[x] += VERegisters[y];
                        pc += 2;
                        break;
                    case 0x0005: //8xy5 Vx - Vy
                        if (VERegisters[y] < (VERegisters[x])) {
                            VERegisters[0xF] = 1; //0xF is 15
                        } else {
                            VERegisters[0xF] = 0;
                        }
                        VERegisters[x] -= VERegisters[y];
                        pc += 2;
                        break;
                    case 0x0006: //8xy6 TODO Check if 8xy6 and 8xyE are alright.
                        VERegisters[F] = (short) (VERegisters[x] & 1);
                        VERegisters[x] = (short) (VERegisters[y] >> 1);
                        pc += 2;
                        break;
                    case 0x0007: //8xy7
                        if (VERegisters[x] < (VERegisters[y])) {
                            VERegisters[0xF] = 1; //0xF is 15
                        } else {
                            VERegisters[0xF] = 0;
                        }
                        VERegisters[x] = (short) (VERegisters[y] - VERegisters[x]);
                        pc += 2;
                        break;
                    case 0x000E: // 8xyE
                        VERegisters[F] = (short) (VERegisters[x] >> 7);
                        VERegisters[x] = (short) (VERegisters[y] << 1);
                        pc += 2;
                        break;
                }
                break;
            case 0x9000:
                if (VERegisters[x] != VERegisters[y]) {
                    pc += 4;
                } else {
                    pc += 2;
                }
                break;
            case 0xA000: // ANNN sets I equal to NNN
                I = nnn;
                pc += 2;
                break;
            case 0xB000: //BNNN sets program counter equal to NNN + V0
                pc = (short) (nnn + VERegisters[0]);
                break;
            case 0xC000: //Cxkk
                Random random = new Random();
                VERegisters[x] = (short) (random.nextInt(256) & kk);
                pc += 2;
                break;
            case 0xD000: //DXYN
                drawGraphics();
                break;
            case 0xE000:
                switch (opCode & 0x00FF) {
                    case 0x009E:
                        if (VERegisters[x] == keyboard.getKey()) {
                            pc += 4;
                        } else {
                            pc += 2;
                        }
                        break;
                    case 0x00A1:
                        if (VERegisters[x] != keyboard.getKey()) {
                            pc += 4;
                        } else {
                            pc += 2;
                        }
                        break;
                    default:
                        System.out.printf("Unknown OPCode %s", opCode);
                        break;
                }
                break;
            case 0xF000:
                switch (opCode & 0x00FF) {
                    case 0x0007: //Fx07
                        VERegisters[x] = delayTimer;
                        pc += 2;
                        break;
                    case 0x000A: // Fx0A LD Key press into Vx.
                        if (keyboard.getKey() == -1) {
                            return;
                        }
                        VERegisters[x] = keyboard.getKey();
                        pc += 2;
                        break;
                    case 0x0015: //Fx15 sets delayTimer equals to Vx.
                        delayTimer = VERegisters[x];
                        pc += 2;
                        break;
                    case 0x0018: //Fx18 Sets soundTimer equals to Vx
                        soundTimer = VERegisters[x];
                        pc += 2;
                        break;
                    case 0x001E: //Fx1E
                        if ((I + VERegisters[x]) > 0xFFF) {
                            VERegisters[F] = 1;
                        } else {
                            VERegisters[F] = 0;
                        }
                        I += VERegisters[x];
                        pc += 2;
                        break;
                    case 0x0029: //Fx29
                        I = (short) (VERegisters[x] * 0x5);
                        pc += 2;
                        break;
                    case 0x0033: //Fx33
                        storeBinaryRepresentation();
                        pc += 2;
                        break;
                    case 0x0055: //Fx55
                        storeVXIntoMemory();
                        pc += 2;
                        break;
                    case 0x0065: //Fx65
                        readVXFromMemory();
                        pc += 2;
                        break;
                }
                break;
        }
    }

    private void storeBinaryRepresentation() {
        int x = getVx();
        memory.write(VERegisters[x] / 100, I);
        memory.write((VERegisters[x] / 10) % 10, I + 1);
        memory.write((VERegisters[x] % 100) % 10, I + 2);
    }

    private void storeVXIntoMemory() {
        for (int i = 0; i <= getVx(); i++) {
            memory.write(VERegisters[i], I + i);
        }
    }

    private void readVXFromMemory() {
        for (int i = 0; i <= getVx(); i++) {
            VERegisters[i] = memory.read(I + i);
        }
    }

    private void drawGraphics() {
        short x = VERegisters[(opCode & 0x0F00) >> 8];
        short y = VERegisters[(opCode & 0x00F0) >> 4];
        short height = (short) (opCode & 0x000F);
        short pixel;
        VERegisters[0xF] = 0;

        for (int yLine = 0; yLine < height; yLine++) {
            pixel = memory.read(I + yLine);
            for (int xLine = 0; xLine < 8; xLine++) {
                if ((pixel & (0x80 >> xLine)) != 0) {
                    short trueX = (short) ((x + xLine) % 64);
                    short trueY = (short) ((y + yLine) % 32);
                    if (screen.screenSpace[trueX + trueY] == 1) {
                        VERegisters[0xF] |= screen.screenSpace[(x + xLine + ((y + yLine) * 64)) % 2048] == 1 ? 1 : 0;
                    }
                    screen.screenSpace[(x + xLine + ((y + yLine) * 64)) % 2048] ^= 1;
                }
            }
        }
        drawFlag = true;
        pc += 2;
    }
}
