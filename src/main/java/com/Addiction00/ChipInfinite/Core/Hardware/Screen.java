package com.Addiction00.ChipInfinite.Core.Hardware;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;

public class Screen {
    private final static int WIDTH = 64;
    private final static int HEIGHT = 32;
    private final int scale = 10;
    public short[] screenSpace = new short[WIDTH * HEIGHT];

    void clear() {
        for(int  i = 0; i < WIDTH * HEIGHT; i++){
            screenSpace[i] = 0;
        }
    }

    private boolean whitePixel(short pixel) {
        return pixel != 0;
    }

    public void draw(Canvas canvas) {
        PixelWriter pw = canvas.getGraphicsContext2D().getPixelWriter();
        for (int i = 0; i < HEIGHT * scale; i++) {
            for (int j = 0; j < WIDTH * scale; j++) {

                if (whitePixel(screenSpace[((i / scale) * WIDTH) + (j / scale)])) {
                    pw.setColor(j, i, Color.WHITE);
                } else {
                    pw.setColor(j, i, Color.BLACK);
                }
            }
        }
    }

    public short[] getScreenSpace() {
        return screenSpace;
    }
}