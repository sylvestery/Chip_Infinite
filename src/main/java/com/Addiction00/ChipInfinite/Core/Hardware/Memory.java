package com.Addiction00.ChipInfinite.Core.Hardware;

/**
 * @author Yashiyah Sylvester
 */
public class Memory {
    private static final int SIZE_4K = 0x1000;
    public static final short[] FONT = {
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x50, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80  // F
    };  // Initial Font loading.
    private short[] memory;

    private Memory(int size) {
        this.memory = new short[size];
    }

    public Memory() {
        this(SIZE_4K);
    }

    public short read(int i) {
        if (i >= 0 && i < memory.length) {
            return (short) (memory[i] & 0xFF);
        } else {
            throw new IllegalArgumentException("location must be 0 or larger.");
        }
    }

    public void loadBytesIntoMemory(byte[] data, int offset) {
        int currentOffset = offset;
        for (byte nextByte : data) {
            write(nextByte, currentOffset);
            currentOffset++;
        }
    }

    public void write(int newByte, int i) {
        if (i > memory.length || i < 0) {
            throw new IllegalArgumentException("Location is not within size.");
        } else {
            memory[i] = (short) (newByte & 0xFF);
        }
    }

}
