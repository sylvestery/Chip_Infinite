package com.Addiction00.ChipInfinite.Core.Emulator;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;

public class EmulatorRunnable implements Runnable{
    private Scene  scene;
    private Canvas canvas;
    private Emulator emulator;

    public EmulatorRunnable(Scene scene, Canvas canvas, Emulator emulator) {
        this.scene = scene;
        this.canvas = canvas;
        this.emulator = emulator;
    }

    @Override
    public void run() {

        while (canvas.isVisible()) {
            emulator.handleKeyPress(scene);
            emulator.getCpu().emulateCycle();
            emulator.cycleWait();
            if (emulator.getCpu().isDrawFlag()) {
                emulator.getScreen().draw(canvas);
            }
            emulator.getCpu().setDrawFlag(false);

        }
    }
}
