package com.Addiction00.ChipInfinite.Core.Emulator;

import com.Addiction00.ChipInfinite.Core.Hardware.CPU;
import com.Addiction00.ChipInfinite.Core.Hardware.Keyboard;
import com.Addiction00.ChipInfinite.Core.Hardware.Memory;
import com.Addiction00.ChipInfinite.Core.Hardware.Screen;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Addiction (Yasiyah Sylvester)
 */
public class Emulator  {

    private Memory memory;
    private CPU cpu;
    private Screen screen;
    private Keyboard keyboard;
    private int cycleRate;
    private Scene scene;
    private Canvas canvas;
    private String[] args = new String[2];
    //Screen screen = new Hardware.Screen;
    //Keyboard keyboard = new Keyboard;
    public Emulator() {
        this.memory = new Memory();
        this.screen = new Screen();
        this.keyboard = new Keyboard();
        this.cpu = new CPU(memory, screen, keyboard);
        this.cycleRate = 1000 / 600; //About 2 ms per cycle.
    }


    public Emulator(Scene scene, Canvas canvas) {
        this();
        this.scene = scene;
        this.canvas = canvas;
    }

    public void loadRom(String filePath) {
        if (filePath != null) {
            memory.loadBytesIntoMemory(turnFileToByteArray(filePath), CPU.PROGRAM_COUNTER_START);
        }
    }

    public void loadRom(File file) {
        loadRom(file.getPath());
    }

    public boolean checkIfRomIsLoaded() {
        if (memory.read(0x200) != 0) {
            System.out.println("Rom is loaded");
            return true;
        }
        return false;
    }

    //TODO Converts all the file paths.
    private byte[] turnFileToByteArray(String filePath) {
        try {
            return Files.readAllBytes(Paths.get(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Screen getScreen() {
        return screen;
    }

    public CPU getCpu() {
        return cpu;
    }

    void cycleWait() {
        long differenceInCycle = System.currentTimeMillis() - cpu.getCpuTime();
        if (differenceInCycle < cycleRate) {
            try {
                Thread.sleep(cycleRate - differenceInCycle);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        cpu.setCpuTime(System.currentTimeMillis());

    }

    public void handleKeyPress(Scene scene) {
        keyboard.setKeyWithPress(scene);
        keyboard.setKeyRelease(scene);
       // keyboard.debugKeys(scene, this.args);
    }


    public void start() {
        cpu.setFontSpace(Memory.FONT);
    }



}

